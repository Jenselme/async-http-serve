#!/usr/bin/env python3

import os

from setuptools import (
    find_packages,
    setup,
)


here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst')) as f:
    README = f.read()


setup(
    name='ahttpserve',
    version='0.5.0',
    description='Small Python 3.6+ script that rely on aiohttp and aiofiles to asynchronously serve static files.',
    long_description=README,
    classifiers=[
        'Programming Language :: Python :: 3',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Topic :: Internet :: WWW/HTTP :: HTTP Servers',
    ],
    entry_points={
        'console_scripts': [
            'ahttpserve=ahttpserve.__main__:main',
        ],
    },
    author='Julien Enselme',
    author_email='jenselme@jujens.eu',
    url='https://gitlab.com/Jenselme/async-http-serve',
    keywords='aiohttp aiofiles asyncio http',
    license='MIT',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'aiohttp > 2.3,<3.0',
        'aiofiles > 0.3.0, <1.0',
    ],
)
