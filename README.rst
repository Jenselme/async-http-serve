Async HTTP Serve
================

Small Python 3.6+ script that rely on [aiohttp](https://github.com/aio-libs/aiohttp) and [aiofiles](https://github.com/Tinche/aiofiles) to asynchronously serve static files. This is meant to be an asynchronous version of ``python3 -m http.server``.

Use it like ``ahttpserve -p PORT`` to serve the current directory at ``localhost:PORT``.
